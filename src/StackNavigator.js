import React, {useEffect} from 'react';
import {createAppContainer, createSwitchNavigator} from 'react-navigation'
import {createStackNavigator} from 'react-navigation-stack'
import {createDrawerNavigator} from 'react-navigation-drawer'
import { SafeAreaView , SafeAreaProvider} from 'react-native-safe-area-context'
// import {Provider} from 'react-redux'
// import {createStore} from 'redux'
// import reducers from './src/reducers'
// import * as global from './src/global'
// import {initialize, loadCarttoReducer} from './src/BaaS/BaaSFunctions'

import SplashScreen from './screens/SplashScreen'
import LanguageSelectionScreen from './screens/LanguageSelectionScreen'
import WelcomeMessageScreen from './screens/WelcomeMessageScreen'
import AboutAppScreen from './screens/AboutAppScreen'
import LocationPermissionsScreen from './screens/LocationPermissionsScreen'
import AppPolicyScreen from './screens/AppPolicyScreen'
import NewUserLoginScreen from './screens/NewUserLoginScreen'
import ReturningUserLoginScreen from './screens/ReturningUserLoginScreen'
import AppHomeScreen from './screens/AppHomeScreen'
import SettingsScreen from './screens/SettingsScreen'
import SymptomsReportingScreen from './screens/SymptomsReportingScreen'
import TestResultsReportingScreen  from './screens/TestResultsReportingScreen ';
import SymptomsResponseScreen from './screens/SymptomsResponseScreen '
import TestResultsResponseScreen from './screens/TestResultsResponseScreen'
import SupportUsScreen from './screens/SupportUsScreen'
import FirstTimeQuestionnaireScreen from './screens/FirstTimeQuestionnaireScreen'

// import testScreen from './screens/testScreen'

import CustomDrawer from './components/CustomDrawer'

const navigator = createSwitchNavigator(
    {
      Splash: SplashScreen,
      
      firstTimeFlow: createSwitchNavigator({
        LanguageSelection: LanguageSelectionScreen,  
        WelcomeMessage: WelcomeMessageScreen,
        AboutApp: AboutAppScreen,
        LocationPermissions: LocationPermissionsScreen,
        AppPolicy: AppPolicyScreen,
      },
      {
        headerMode: false
      }),
      loginFlow: createStackNavigator({
        ReturningUserLogin: ReturningUserLoginScreen,
        NewUserLogin: NewUserLoginScreen,
        FirstTimeQuestionnaire: FirstTimeQuestionnaireScreen,
      },
      {
        headerMode: false
      }),
      mainFlow: createStackNavigator({
        drawerFlow: createDrawerNavigator({
          AppHome: AppHomeScreen,
          Settings: SettingsScreen,
          SymptomsReporting: SymptomsReportingScreen,
          TestResultsReporting: TestResultsReportingScreen,
          SymptomsResponse: SymptomsResponseScreen,
          TestResultsResponse: TestResultsResponseScreen,
          SupportUs: SupportUsScreen,

          LanguageSelectionDrawer: LanguageSelectionScreen,  
          AboutAppDrawer: AboutAppScreen,
          LocationPermissionsDrawer: LocationPermissionsScreen,
          AppPolicyDrawer: AppPolicyScreen,
        },
        {
          contentComponent: CustomDrawer
        }),       
      },
      {
        headerMode: false
      }),  
    },
    {
      headerMode: false,
      initialRouteName: 'Splash',
    }
)
const AppContainer =  createAppContainer(navigator)

const StackNavigator = () => {
  return (
    // <Provider store={createStore(reducers)}>
      <SafeAreaProvider>
        <SafeAreaView style={{flex:1}}>
          <AppContainer/>
        </SafeAreaView>
      </SafeAreaProvider>
    // </Provider>
  )
}

export default StackNavigator























// const navigatorA = createSwitchNavigator(
//     {
//       Welcome: WelcomeScreen,
//       signingFlow: createStackNavigator(
//       {
//         Signin: SigninScreen,
//         Signup: SignupScreen,
//       },
//       {
//         headerMode: false
//       }),
        
//       mainFlow: createStackNavigator(
//       {
//         drawerFlow: createDrawerNavigator(
//         {
//           Home: HomeScreen,
//         },
//         {
//           contentComponent: CustomDrawer
//         }), 
//         Cart: CartScreen,
//         ItemDetails: ItemDetailsScreen,
//         OrdersHistory: OrdersHistoryScreen,
//         PromoAletrs: PromoAletrsScreen,
//         Faqs: FaqsScreen,
//         Checkout: CheckoutScreen,
//         // CardForm: CardFormScreen    
//       },
//       {
//         headerMode: false
//       })
//     },
//     {
//       initialRouteName: 'Welcome',
//     })
    