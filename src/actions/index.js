export const loadUIElements = (values) => {
    return {
        type: 'set_UIElements',
        payload: values
    }
}