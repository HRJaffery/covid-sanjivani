import axios from 'axios'
import  {AsyncStorage} from 'react-native'
import * as configs from '../configs'
var Hashes = require('jshashes')

let webViewBase
let webViewBaseWithKey
let authorization
let authkeyHash
let allHash
let minusPasswordHash
export let username
export let userUUID4
export let password
export let language
export let AboutAppWebView
export let LocationPermissionsWebView
export let AppPolicyWebView
export let FirstTimeQuestionnaireWebView
export let SymptomsResponseWebView
export let TestResultsResponseWebView
export let SupportUsWebView
export let AppHomeWebView
let lat
let long
let locationPermissionVariable

export const init = async () => {

    lat = await configs.getLatitude()
    long = await configs.getLongitude()
    username = await configs.getUsername()
    userUUID4 = await configs.getuserUUID4()
    password = await configs.getPassword()
    language = await configs.getLanguage()
    // console.log('u',username)
    let x = await lat + long
    if(lat && long){
        locationPermissionVariable = 1
    } else {
        locationPermissionVariable = 0
    }

    // await console.log(lat,long, username,"latlongggg")
    let key = await userUUID4 + username + password + configs.authkey
    // await console.log("u",username,"UU",userUUID4, "password",password,"l",language,'lat',lat,"long",long)
    authorization = await new Hashes.SHA256().hex(key)
    authkeyHash = await new Hashes.SHA256().hex(configs.authkey)
    minusPasswordHash = await new Hashes.SHA256().hex(userUUID4 + username + configs.authkey)
    allHash = await new Hashes.SHA256().hex(key)

    // console.log("hashhhhhh",authkeyHash)

    // <SHA256(<userUUID4><Username><Password><authkey>)>

    webViewBaseWithKey = await "https://658covidsanjivaniapi.com/ui/get-ui-webpage?authorization="+authorization+"&userUUID4="+userUUID4+"&app-language="+language+"&unique-ui-element-id="
    webViewBase = await "https://658covidsanjivaniapi.com/ui/get-ui-webpage?authorization="+authkeyHash+"&userUUID4="+userUUID4+"&app-language="+language+"&unique-ui-element-id="
    
    AboutAppWebView = await webViewBase + "AboutAppScreen-UIElement-1"
    LocationPermissionsWebView = await webViewBase + "LocationPermissionsScreen-UIElement-1"
    AppPolicyWebView = await webViewBase + "AppPolicyScreen-UIElement-1"
    FirstTimeQuestionnaireWebView = await webViewBaseWithKey + "FirstTimeQuestionnaireScreen-UIElement-1"
    SymptomsResponseWebView = await webViewBaseWithKey + "SymptomsResponseScreen-UIElement-1"
    TestResultsResponseWebView = await webViewBaseWithKey + "TestResultsResponseScreen-UIElement-1"
    SupportUsWebView = await webViewBase + "SupportUsScreen-UIElement-1"
    AppHomeWebView = await "https://658covidsanjivaniapi.com/ui/get-ui-webpage?authorization="+authorization+"&userUUID4="+userUUID4+"&app-language="+language+"&unique-ui-element-id=AppHomeScreen-UIElement-1&latitude=" + lat + "&longitude=" + long + "&users-location-permission-preference=" + locationPermissionVariable
    
}
init()

export const getAllUIElements =  async () => {
    let date = new Date
    
    const config = {
        method: 'post',
        url: "https://658covidsanjivaniapi.com/ui/get-all-ui-element-details",
        headers: {
            "Authorization": authkeyHash,
            "Content-Type" : "application/json"
        }, 
        data: {
            "time": date,
            "userUUID4": null,
            "Username": null,
            "platform-os": configs.platform,
            "os-version" : configs.platformVersion,
            "app-version": "1.0.00",
            "device": configs.model,  
            "pagename": "SplashScreen",
            "app-language": language
        }
    }
    try {
        let res = await axios(config)
        await AsyncStorage.setItem('UIElements',JSON.stringify(res.data))
        return res.data
    } catch (error) {
        return -1
    }   
}

export const reportTestResults =  async (list, userSelected, userUnselected, code=null) => {
    
    let date = new Date
    const config = {
        method: 'post',
        url: "https://658covidsanjivaniapi.com/reporting/report-users-test-results",
        headers: {
            "Authorization": authorization,
            "Content-Type" : "application/json"
        }, 
        data: {
            "time":date,
            "userUUID4": userUUID4,
            "Username": username,
            "platform-os": configs.platform,
            "os-version" : configs.platformVersion,
            "app-version": "1.0.00",
            "device": configs.model,  
            "pagename": "TestResultsReportingScreen",
            "app-language": language,
            "user-test-results": {
              "all-shown": list,
              "user-selected" : userSelected,
              "user-unselected" : userUnselected,
              "test-code": code
            }
          }
    }
    try {
        let res = await axios(config)
        // console.log(res.data.reported)
        return res.data.reported
    } catch (error) {
        return -1
    }   
}

export const reportSymptoms =  async (list, userSelected, userUnselected) => {
    
    let date = new Date
    
    const config = {
        method: 'post',
        url: "https://658covidsanjivaniapi.com/reporting/report-users-symptoms",
        headers: {
            "Authorization": authorization,
            "Content-Type" : "application/json"
        }, 
        data: {
            "time": date,
            "userUUID4": userUUID4,
            "Username": username,
            "platform-os": configs.platform,
            "os-version" : configs.platformVersion,
            "app-version": "1.0.00",
            "device": configs.model,  
            "pagename": "SymptomsReportingScreen",
            "app-language": language,
            "symptoms": {
              "all-shown": list,
              "user-selected" : userSelected,
              "user-unselected" : userUnselected
            }
        }
    }
    try {
        let res = await axios(config)
        
        return res.data.reported
    } catch (error) {
        // console.log(res)
        return -1
        
    }   
}

export const signupUser =  async () => {
    // console.log(authkeyHash)
    let date = new Date
    const config = {
        method: 'post',
        url: "https://658covidsanjivaniapi.com/users/signup-anonymous-user",
        headers: {
            "Authorization": authkeyHash,
            "Content-Type" : "application/json"
        }, 
        data: {
            "time": date,
            "userUUID4": null,
            "Username": null,
            "platform-os": configs.platform,
            "os-version" : configs.platformVersion,
            "app-version": "1.0.00",
            "device": configs.model,  
            "pagename": "NewUserLoginScreen",
            "app-language": language,
        }
    }
    try {
        let res = await axios(config)
        if(res.data['signup-status'] !== "failure"){
            
            let credentials = res.data.credentials
            await AsyncStorage.setItem('userUUID4',credentials['userUUID4'])
            await AsyncStorage.setItem('Username',credentials['Username'])
            await AsyncStorage.setItem('Password',credentials['Password'])
            await init()
            return credentials
        }
        // console.log(credentials)
        return -1
    } catch (error) {
        return -1
    }   
}


export const loginUser =  async (username, password) => {
    // console.log(authkeyHash)
    // console.log(username)
    // console.log(password)
    let date = new Date
    const config = {
        method: 'post',
        url: "https://658covidsanjivaniapi.com/users/login-by-username-password",
        headers: {
            "Authorization": authkeyHash,
            "Content-Type" : "application/json"
        }, 
        data: {
            "time": date,
            "Username": username,
            "Password": password,
            "platform-os": configs.platform,
            "os-version" : configs.platformVersion,
            "app-version": "1.0.00",
            "device": configs.model,  
            "pagename": "ReturningUserLoginScreen",
            "app-language": language,
        }
    }
    try {
        let res = await axios(config)
        // console.log(res.data)
        if (res.data['login-status'] !== "failure"){
            
            await AsyncStorage.setItem('userUUID4',res.data['userUUID4'])
            await AsyncStorage.setItem('Username',username)
            await AsyncStorage.setItem('Password',password)
            await init()
        }
        
        
        return res.data['login-status']
    } catch (error) {
        return -1
    }   
}

export const sendLocationData =  async (pagename, locationReadSource, lat, long) => {
    // console.log('sendlocationdata')
    let date = new Date
    let key = userUUID4 + username + configs.authkey
    // console.log(key)
    const authorization = new Hashes.SHA256().hex(key)
    
    const config = {
        method: 'post',
        url: "https://7vh57re0ug.execute-api.us-east-1.amazonaws.com/prod_env/add-user-location",
        headers: {
            "Authorization": authorization,
            "Content-Type" : "application/json"
        }, 
        data: {
            "Data":{
                "authorization": authorization ,
                "time":date,
                "userUUID4": userUUID4,
                "Username": username,
                "platform-os": configs.platform,
                "os-version" : configs.platformVersion,
                "app-version": "1.0.00",
                "device": configs.model, 
                "pagename":pagename,
                "app-language":language,
                "users-location-permision-preference":0,
                "location-read-source":locationReadSource,
                "location-read-type":"best-available-to-os",
                "latitude":lat,
                "longitude":long
             },
             "PartitionKey":long
            }
        
    }
    try {
        let res = await axios(config)
        // console.log(res.data)

    } catch (error) {
        // console.log(error)
        return -1
    }   
}