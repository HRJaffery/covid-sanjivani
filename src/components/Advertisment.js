import React from 'react'
import {Platform} from 'react-native'
import {addunitIDAndroidTop, addunitIDAndroidBottom, addunitIDiOSTop, addunitIDiOSBottom} from '../configs'
import {AdMobBanner} from 'react-native-admob';

const Advertisment  = ({position}) => {
    // console.log(Platform.OS)
    return (
        <>
        {Platform.OS === 'ios'
        ?
            <>
                {position === 'top'
                ?
                    <AdMobBanner
                    adSize="SMART_BANNER"
                    adUnitID={addunitIDiOSTop}
                    testDeviceID="EMULATOR"
                    // onAdFailedToLoad={(error) => console.error(error)}
                    />
                :
                    <AdMobBanner
                    adSize="SMART_BANNER"
                    adUnitID={addunitIDiOSBottom}
                    testDeviceID="EMULATOR"
                    // onAdFailedToLoad={(error) => console.error(error)}
                    />
                } 
            </>
        :
            <>
                {position === 'top'
                ?
                    <AdMobBanner
                    adSize="SMART_BANNER"
                    adUnitID={addunitIDAndroidTop}
                    testDeviceID="EMULATOR"
                    // onAdFailedToLoad={(error) => console.error(error)}
                    />
                :
                    <AdMobBanner
                    adSize="SMART_BANNER"
                    adUnitID={addunitIDAndroidBottom}
                    testDeviceID="EMULATOR"
                    // onAdFailedToLoad={(error) => console.error(error)}
                    />
                }
                </>
        }
        </>
    )
}

export default Advertisment

