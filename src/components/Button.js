import React from 'react'
import { Text, StyleSheet, TouchableOpacity, ActivityIndicator } from 'react-native'

const Button = ({buttonText, callback, btnWidth, loading}) => {
    return (
      <>
      {!loading
      ?
      <>
        { !btnWidth
          ?
        <TouchableOpacity onPress={()=>{callback()}} style={styles.button }>
              <Text style={styles.buttonText}>{buttonText}</Text>
          </TouchableOpacity>
          :
          <TouchableOpacity onPress={()=>{callback()}} style={[styles.button, {width: btnWidth} ]}>
              <Text style={styles.buttonText}>{buttonText}</Text>
          </TouchableOpacity>
        
        }
        </>
        : <TouchableOpacity onPress={()=>{callback()}} style={[styles.button, {width: btnWidth} ]}>
        <ActivityIndicator
            
            color="white"
          />
    </TouchableOpacity>

      }
      </>
    )
}

const styles = StyleSheet.create({
    

button: {
    height: 50, 
    borderWidth: 1, 
    borderColor: 'white', 
    borderRadius: 20, 
    justifyContent: 'center', 
    alignItems:'center',
    backgroundColor: '#3b5998'
  },
  buttonText: {
    color: 'white', 
    fontSize:18,
    paddingHorizontal:20,
    paddingBottom:4
  },

})

export default Button
