import React from 'react'
import { View, Text, StyleSheet, TouchableOpacity, AsyncStorage} from 'react-native'
import LogoHeader from './LogoHeader'

import DrawerItem from './DrawerItem'

const  CustomDrawer = ({navigation}) => {
 
    const AppHome = ()=>{
        navigation.closeDrawer()
        navigation.navigate('AppHome')
    }
    const SymptomsReporting = ()=>{
        navigation.closeDrawer()
        navigation.navigate('SymptomsReporting')
    }
    const TestResultsReporting = ()=>{
        navigation.closeDrawer()
        navigation.navigate('TestResultsReporting')
    }
    const LocationPermissions = ()=>{
        navigation.closeDrawer()
        navigation.navigate('LocationPermissionsDrawer')
    }
    const AboutApp = ()=>{
        navigation.closeDrawer()
        navigation.navigate('AboutAppDrawer')
    }
    const AppPolicy = ()=>{
        navigation.closeDrawer()
        navigation.navigate('AppPolicyDrawer')
    }
    const Settings = ()=>{
        navigation.closeDrawer()
        navigation.navigate('Settings')
    }
    const SupportUs = ()=>{
        navigation.closeDrawer()
        navigation.navigate('SupportUs')
    }
    const logout = ()=>{
        navigation.closeDrawer()
        navigation.navigate('Signin')
        AsyncStorage.removeItem('token')
        AsyncStorage.removeItem('uid')
    }
    return (
       
        <View style={styles.main}>
            
            <View style={styles.banner}>
                <LogoHeader/>
                {/* <Text  style={styles.bannerText}>Covid-Sanjivani</Text> */}
            </View> 
            
            <DrawerItem icon='home' label='Home' callback={AppHome}/>
            <DrawerItem icon='shopping-cart' label='Report Symptoms' callback={SymptomsReporting}/>
            <DrawerItem icon='history' label='Report Test Results' callback={TestResultsReporting}/>
            <DrawerItem icon='bell-outline' iconLib='MaterialCommunityIcons' label='Why App Needs Location' callback={LocationPermissions}/>
            <DrawerItem icon='help-outline' label='About App' callback={AboutApp}/>
            <DrawerItem icon='help-outline' label='App Policy' callback={AppPolicy}/>
            <DrawerItem icon='help-outline' label='Settings' callback={Settings}/>
            <DrawerItem icon='help-outline' label='Support Us' callback={SupportUs}/>
            <View style={styles.space}>
            </View>        
            
            {/* <DrawerItem icon='logout' iconLib='MaterialCommunityIcons' label='Logout' callback={logout}/> */}
           
        </View>
    )
}

const styles = StyleSheet.create({
    banner: {
        height: 180,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 10
    },
    bannerText: {
        color: 'white',
        fontSize: 80
    },
    main: {
        flex: 1
    },
    space: {
        flex: 1   
    },
    text: {
        fontSize: 14,
        fontWeight: 'bold',
        marginLeft: 5
    },
    textOpacity: {
        flexDirection: 'row',
        height: 50,
        alignItems: 'center',
        paddingHorizontal: 15,
    }
})

export default CustomDrawer