import React from 'react'
import {Text, StyleSheet, TouchableOpacity} from 'react-native'
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

export default function DrawerItem  ({callback, icon, iconLib, label}) { 
    return (
        <TouchableOpacity style={styles.textOpacity} onPress={()=>callback()}>
           {/* {(iconLib === 'MaterialCommunityIcons') 
            ?   <MaterialCommunityIcons name={icon} size={20} />
            :   <MaterialIcons name={icon} size={20} />
            } */}
            
            <Text style={styles.text}>{label}</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    
    text: {
        fontSize: 14,
        fontWeight: 'bold',
        marginLeft: 5
    },
    textOpacity: {
        flexDirection: 'row',
        height: 50,
        alignItems: 'center',
        paddingHorizontal: 15,
        // borderEndColor: '#3b5998',
        // borderBottomWidth: 1
        

    }
})