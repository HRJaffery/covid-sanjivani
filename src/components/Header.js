import React from 'react'
import { View, Text, StyleSheet, TouchableOpacity, Keyboard} from 'react-native'
import MaterialIcons from "react-native-vector-icons/MaterialIcons";

const Header = ({leftIcon,leftCallback}) => {
return (
    <>
    
    <View style={styles.header}>
        {leftIcon ?
        <TouchableOpacity style={{marginRight: 10, padding: 5}} 
        onPress={()=>{
            Keyboard.dismiss()
            leftCallback()
        }}>
            <MaterialIcons name={leftIcon} color='#3b5998' size={30}/>
        </TouchableOpacity>
        : null
        }
        
    </View>
    
    </>
    )
}

const styles = StyleSheet.create({
    view: {
        flexDirection: 'row',
        height: 50,
        alignItems: 'center',
        padding: 5,
        backgroundColor: 'white',
        shadowColor: '#000',
        shadowOffset: {width: 0, height: 2},
        shadowOpacity: 0.2,
        elevation: 2,
  
    },
    header: {
        flexDirection: 'row',
        height: 50,
        alignItems: 'center',
        padding: 5,
        backgroundColor: 'white',
        shadowColor: '#000',
        shadowOffset: {width: 0, height: 2},
        shadowOpacity: 0.2,
    },
    rightIcon: {
        padding: 0,
        marginRight: 5,
        paddingVertical:5,
        paddingLeft:8,
        paddingRight: 3,
    }
})

export default Header