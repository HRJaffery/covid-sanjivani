import React, { Component } from "react";
import { StyleSheet, View, AsyncStorage} from "react-native";
import SectionedMultiSelect from './react-native-sectioned-multi-select-master'
import {connect} from 'react-redux'

const items = [
  // this is the parent or 'item'
  {
    name: 'Languages',
    id: 0,
    // these are the children or 'sub items'
    children: [
      {
        name: 'English',
        id: 10,
      }
      
    ],
  }
];

class LanguageDropdown extends Component {

  constructor(props) {
    super(props);
    this.state = {
      selectedItems: [],
      languages: this.createLanguagesArray()
    };
  }
  
  onSelectedItemsChange = async(selectedItems) => {
    this.setState({ selectedItems });
    let list = this.props.languages
    await AsyncStorage.setItem("preferred-language",list[selectedItems])
  };
  
  createLanguagesArray = () => {
    let list = this.props.languages
    let children = []
    let i = 0
    list.forEach(element => {
      let obj = {}
      obj["name"] = element
      obj["id"] = i
      i = i+1
      children.push(obj)
    });
    let languages = [
      {
        name: "Languagess",
        id: -1,
        children: children
      }
    ]
    // console.log(children)
    return languages
  }

  render() {
    return (
      <View style={styles.view}>
       
        
        <SectionedMultiSelect
          items={this.state.languages}
          hideSearch = {true}
          uniqueKey="id"
          subKey="children"
          selectText="English"
          showDropDowns={false}
          readOnlyHeadings={true}
          onSelectedItemsChange={this.onSelectedItemsChange}
          selectedItems={this.state.selectedItems}
          showChips={false}
          single = {true}
          styles = {styles}
        
        />

      </View>
    );
  }
}
const styles = StyleSheet.create({
  view: {
    marginVertical: 10,
    marginHorizontal: 55,
  },
  container: {
    marginTop: 120,
  },

})

export default LanguageDropdown
