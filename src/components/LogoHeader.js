import React from 'react'
import { View, StyleSheet, Image} from 'react-native'

const LogoHeader = () => {
    return (
        <View style={styles.view}>
            <Image style={styles.image} source = {require('../../assets/App-Logo.png')} />
        </View>
    )
}

const styles = StyleSheet.create({
    view: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    image: {
        width: '100%',
        height: '100%',
        aspectRatio: 1,
    }
})

export default LogoHeader