import {AsyncStorage, Platform} from 'react-native'
import DeviceInfo from 'react-native-device-info';
import Geolocation from '@react-native-community/geolocation';
import {check, PERMISSIONS} from 'react-native-permissions';

// check(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION)
//     .then((res)=>{
//         if(res === "denied"){
//             //Donothing
//         } else {
//             Geolocation.getCurrentPosition(
//                 info => {
//                     var lat =  info.coords.latitude
//                     var long = info.coords.longitude
//                     console.log("x  ",lat,long)
//                 }
//             );
//         } 
// })

// export const lat = 

export const getLongitude = async() => {
   
    return JSON.parse(await AsyncStorage.getItem("long")   )
}
export const long = getLongitude()

export const getLatitude = async() => {
    return JSON.parse(await AsyncStorage.getItem("lat") )
}
export const lat = getLatitude()

export const getLanguage = async() => {
    // const appLanguage = await AsyncStorage.getItem("preferred-language") 
    // return appLanguage
    return "English"
}
export const language = getLanguage()

export const getUsername = async() => {
    return await AsyncStorage.getItem("Username") 
}
export const username = getUsername()

export const getuserUUID4 = async() => {
    return await AsyncStorage.getItem("userUUID4") 
}
export const userUUID4 = getuserUUID4()

export const getPassword = async() => {
    return await AsyncStorage.getItem("Password") 
}
export const password = getPassword()

export const platform = Platform.OS
export const platformVersion = Platform.Version
export const model = DeviceInfo.getDeviceId()
// export const userUUID4 = getuserUUID4()
// export const username = getUsername()
export const authkey = '$c660295a37304c11b5c045d32937459792fa900c956643a9ac611f18cd13523d'


// Android App id: ca-app-pub-7808265846149045~6513450922
export const addunitIDAndroidTop = "ca-app-pub-7808265846149045/6505712066"
export const addunitIDAndroidBottom = "ca-app-pub-7808265846149045/6745933954"

// iOS App id: ca-app-pub-7808265846149045~8797382225
export const addunitIDiOSTop = "ca-app-pub-7808265846149045/5224968721"
export const addunitIDiOSBottom = "ca-app-pub-7808265846149045/6314140373"