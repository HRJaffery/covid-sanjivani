import UIElements from '../languages/english.json'  

export default (state = UIElements, action) => {
    switch (action.type){
        case 'set_UIElements':
            return action.payload
        default:
            return state
    }
}