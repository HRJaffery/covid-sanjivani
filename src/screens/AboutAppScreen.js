import React, {useState} from 'react';
import {View, StyleSheet, ActivityIndicator, Dimensions, TouchableOpacity, Text} from 'react-native';
import {WebView} from 'react-native-webview';
import Header from '../components/Header';
import Button from '../components/Button';
import {AboutAppWebView} from '../api/covidsanjivaniApi';
import {connect} from 'react-redux'
import * as actions from '../actions'
import Advertisment from '../components/Advertisment'
import MaterialIcons from "react-native-vector-icons/MaterialIcons";

const AboutAppScreen = ({navigation,UIElements}) => {
  // console.log(UIElements)
  const button = UIElements['AboutAppScreen-UIElement-2']['content']['text-to-fill']
  const INJECTEDJAVASCRIPT = "document.body.style.userSelect = 'none'";
  const [show, setShow] = useState(false);
  const [LoadingError, setLoadingError] = useState(false)
  const showButton = navigation.getParam('showButton');
  return (
    <View style={styles.view}>
      <View style={styles.advertismentTop}>
      <Advertisment position="top"/>
      </View>
      {!showButton ? (
        <View style={styles.header}>
          <Header
            leftIcon="menu"
            leftCallback={() => navigation.openDrawer()}
          />
        </View>
      ) : null}

      <View style={styles.container}>
      {!LoadingError
      ?
      <>
        <WebView
          source={{uri: AboutAppWebView}}
          onLoad={() => setShow(true)}
          injectedJavaScript={INJECTEDJAVASCRIPT}
          onError = {()=>{setLoadingError(true)}}
        />
        {show ? (
          showButton ? (
            <View style={styles.button}>
              <Button
                buttonText={button}
                btnWidth={230}
                callback={() => {
                  navigation.navigate('LocationPermissions', {
                    showButton: true,
                  });
                }}
              />
            </View>
          ) : null
        ) : (
          <ActivityIndicator
            style={styles.loading}
            size="large"
            color="#3b5998"
          />
        )}
        </>
         :  <View style={{justifyContent: 'center', alignItems:'center', paddingHorizontal: 35}}>
              <Text style={{textAlign: 'center'}}>Something went wrong. Please check your internet connection and try again.</Text>
              <TouchableOpacity style={{marginRight: 10, padding: 5}} 
                        onPress={()=>{
                            setLoadingError(false)
                        }}>
                            <MaterialIcons name='refresh' color='#3b5998' size={30}/>
                        </TouchableOpacity>
            </View>

     }
      </View>
      <View style={styles.advertismentBottom}>
      <Advertisment/>
      </View>
    </View>
  );
};
var {height, width} = Dimensions.get('window');

const styles = StyleSheet.create({
  view: {
    flex: 1,
    backgroundColor: 'white',
    // alignItems: 'center'
  },
  advertismentTop: {
    flex: 1,
    alignItems: 'center',
  },
  header: {
    // borderColor: 'red',
    // borderWidth: 1,
    flex: 1,
  },
  container: {
    flex: 9,
    justifyContent: 'center'
    // borderColor: 'red',
    // borderWidth: 1,
    // paddingHorizontal: 50,
    // alignItems: 'center'
  },
  loading: {
    position: 'absolute',
    top: height / 3,
    left: width / 2.17,
  },
  button: {
    marginVertical: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  advertismentBottom: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    // borderColor: 'red',
    // borderWidth:1,
  },
});

const mapStateToProps = (state) => {
  // console.log(state)
  return {UIElements: state}
}
export default connect(mapStateToProps,actions)(AboutAppScreen)
