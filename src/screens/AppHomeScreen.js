import React, {useState, useEffect} from 'react'
import { View, Text, StyleSheet, TouchableOpacity, ActivityIndicator, SafeAreaView, Dimensions} from 'react-native'
import { WebView } from 'react-native-webview'
import Header from '../components/Header'
import Button from '../components/Button'
import {AppHomeWebView, sendLocationData } from '../api/covidsanjivaniApi'
import {connect} from 'react-redux'
import * as actions from '../actions'
import Advertisment from '../components/Advertisment'
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import Geolocation from '@react-native-community/geolocation';
import {check, PERMISSIONS} from 'react-native-permissions';
import { NavigationEvents } from 'react-navigation';
// import BackgroundTask from 'react-native-background-task'


let lat = 0
let long = 0
// BackgroundTask.define(() => {
//     check(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION)
//         .then((res)=>{
//             if(res === "denied"){
//                 // showToast('Thank you for reporting. To continue helping communities fight Covid-19, please allow the app to access location. Your data is completely anonymous.')
//             } else {
//                 Geolocation.getCurrentPosition(
//                     info => {
//                         // console.log(info.coords.latitude,info.coords.longitude)
//                         let tempLat =  info.coords.latitude
//                         let templong = info.coords.longitude
//                         if (Math.abs((Math.abs(tempLat) - Math.abs(lat))>0.3) || (Math.abs(Math.abs(templong) - Math.abs(long))>0.3)){
//                             sendLocationData("Background","background",info.coords.latitude,info.coords.longitude)
//                             lat = tempLat
//                             long = templong
//                         }
                        
//                     }
//                 );
//             } 
//         })
//     console.log('Hello from a background task')
//     BackgroundTask.finish()
// })
const AppHomeScreen = ({navigation,UIElements}) => {
    
    // console.log(AppHomeWebView)
    // useEffect(()=>{
    //     // BackgroundTask.schedule({period: 3600,})
    //     
    // },[])
    const sendlocation = () => {
        check(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION)
        .then((res)=>{
            if(res === "denied"){
                // showToast('Thank you for reporting. To continue helping communities fight Covid-19, please allow the app to access location. Your data is completely anonymous.')
            } else {
                Geolocation.getCurrentPosition(
                    info => {
                        sendLocationData("AppHomeScreen","foregroud",info.coords.latitude,info.coords.longitude)
                    }
                );
            } 
        })
    }
    
    const button1 = UIElements['AppHomeScreen-UIElement-2']['content']['text-to-fill']
    const button2 = UIElements['AppHomeScreen-UIElement-3']['content']['text-to-fill']
    const [show, setShow] = useState(true)
    const [reload, setReload] = useState(false)
    const [LoadingError, setLoadingError] = useState(false)
    const INJECTEDJAVASCRIPT = "document.body.style.userSelect = 'none'";
    // console.log(AppHomeWebView)

    return (
        <SafeAreaView style={styles.view}>
            <View style={styles.advertismentTop}>
                <Advertisment position="top"/>
            </View>
            <View style={styles.header}>
               
               <Header leftIcon='menu' leftCallback={()=>navigation.openDrawer()}  />
 
            </View>
            <View style={styles.container}>
            <NavigationEvents            
                onWillFocus={() => {
                    sendlocation()
                    setReload(!reload)

                }}
            />
            <ActivityIndicator style={styles.loading} size="large"  color= '#3b5998'/>
                        
                <View style={styles.webView}>
                    {!LoadingError
                    ?
                    <>
                    <WebView 
                        key={reload}

                        source={{ uri: AppHomeWebView }} 
                        onLoad = {()=>{setShow(false)}}  
                        injectedJavaScript={INJECTEDJAVASCRIPT}
                        onError = {()=>{setLoadingError(true)}}
                    />
                        {show
                        ?
                        <ActivityIndicator style={styles.loading} size="large"  color= '#3b5998'/>
                        : null
                        }
                    </>
                    : <View style={{justifyContent: 'center', alignItems:'center', paddingHorizontal: 35}}>
                        <Text style={{textAlign: 'center'}}>Something went wrong. Please check your internet connection and try again.</Text>
                        <TouchableOpacity style={{marginRight: 10, padding: 5}} 
                        onPress={()=>{
                            setLoadingError(false)
                        }}>
                            <MaterialIcons name='refresh' color='#3b5998' size={30}/>
                        </TouchableOpacity>
                    </View>

                    }
                </View>
                <View style={styles.button}>
                    <Button buttonText={button1} btnWidth={280} callback={()=>{navigation.navigate('SymptomsReporting')}}/>               
                    <Button buttonText={button2}  btnWidth={280} callback={()=>{navigation.navigate('TestResultsReporting')}}/>
                </View>
            </View> 
        
            <View style={styles.advertismentBottom}>
            <Advertisment/>
            </View>
        </SafeAreaView>
    )
}
var {height, width} = Dimensions.get('window')
const styles = StyleSheet.create({
    view: {
        flex:1,
        backgroundColor: 'white'
    },
    advertismentTop: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: 'white'
    },  
    header: {
        // borderColor: 'red',
        // borderWidth: 1,
        flex: 1
    },
    container: {
        flex: 9,
        // borderColor: 'red',
        // borderWidth: 1,
        // paddingHorizontal: 50,
        // alignItems: 'center'
    },
    webView: {
        flex: 3,
        justifyContent: 'center'
        // borderColor: 'red',
        // borderWidth: 1
    },
    loading: {
        position: "absolute", 
        top: height / 4, 
        left: width / 2.17,
    },
    button: {
        flex: 1,
        justifyContent: 'space-evenly',
        alignItems: 'center',
        // borderColor: 'red',
        // borderWidth: 1
    },
    advertismentBottom: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        // borderColor: 'red',
        // borderWidth:1,
      },
})

const mapStateToProps = (state) => {
    // console.log(state)
    return {UIElements: state}
  }
  export default connect(mapStateToProps,actions)(AppHomeScreen)