import React, {useEffect, useState} from 'react';
import {View, Text, StyleSheet, AsyncStorage} from 'react-native';

import LogoHeader from '../components/LogoHeader';
import LanguageDropdown from '../components/LanguageDropdown';
import Button from '../components/Button';
import {connect} from 'react-redux'
import * as actions from '../actions'
import Advertisment from '../components/Advertisment'


const LanguageSelectionScreen = ({navigation,UIElements}) => {
  // console.log(UIElements)
  const showButton = navigation.getParam('showButton');
  
  const text1 = UIElements['LanguageSelectionScreen-UIElement-1']['content']['text-to-fill']
  const languages = UIElements['LanguageSelectionScreen-UIElement-2']['content']['list-to-fill']
  const text2 = UIElements['LanguageSelectionScreen-UIElement-3']['content']['text-to-fill']
  const button = UIElements['LanguageSelectionScreen-UIElement-4']['content']['text-to-fill']
  const [LoadingError, setLoadingError] = useState(false)
  
  return (
    <View style={styles.view}>
      <View style={styles.advertismentTop}>
        <Advertisment position="top"/>
      </View>

      <View style={styles.logo}>
        <LogoHeader />
      </View>
      <View style={styles.container}>
        <View style={styles.top}>
          <Text style={styles.text}>{text1}</Text>
          <LanguageDropdown languages={languages}/>
        </View>
        <View style={styles.space}></View>
        <View style={styles.bottom}>
          <Text style={styles.text}>{text2}</Text>

          <View style={styles.button}>
            {showButton ? (
              <Button
                btnWidth={230}
                buttonText={button}
                callback={() =>
                  navigation.navigate('WelcomeMessage', {showButton: true})
                }
              />
            ) : null}
          </View>
        </View>
      </View>
      <View style={styles.advertismentBottom}>
      <Advertisment/>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  view: {
    flex: 1,
    backgroundColor: 'white',
    // alignItems: 'center'
  },
  advertismentTop: {
    flex: 1,
    alignItems: 'center',
  },
  logo: {
    flex: 3,
    // borderColor: 'red',
    // borderWidth: 1,
  },
  container: {
    flex: 5,
    paddingTop: 20,
    // justifyContent: 'space-between',
    // paddingVertical: 50,
    // alignItems: 'center'
    // borderColor: 'red',
    // borderWidth: 1,
  },
  top: {
    flex: 1,
  },
  space: {
    flex: 1,
  },
  bottom: {
    flex: 2,
    justifyContent: 'space-evenly',
    alignItems: 'center',
    paddingHorizontal: 30,
    // borderColor: 'red',
    // borderWidth:1,
  },
  button: {
    flex: 1,
    marginVertical: 10,
    alignItems: 'center',
  },
  text: {
    textAlign: 'center',
  },
  text2: {
    textAlign: 'center',
  },
  advertismentBottom: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    // borderColor: 'red',
    // borderWidth:1,
  },
});
const mapStateToProps = (state) => {
  // console.log(state)
  return {UIElements: state}
}
export default connect(mapStateToProps,actions)(LanguageSelectionScreen);
