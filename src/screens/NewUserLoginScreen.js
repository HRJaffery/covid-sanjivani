import React, {useEffect,useState} from 'react'
import { View, Text, StyleSheet, AsyncStorage} from 'react-native'
import LogoHeader from '../components/LogoHeader'
import Button from '../components/Button'
import Advertisment from '../components/Advertisment'
import {connect} from 'react-redux'
import * as actions from '../actions'
import {signupUser} from '../api/covidsanjivaniApi'
import NetInfo from "@react-native-community/netinfo"
import {init} from '../api/covidsanjivaniApi'

const NewUserLoginScreen = ({navigation,UIElements}) => {

    const text1 = UIElements['NewUserLoginScreen-UIElement-1']['content']['text-to-fill']
    const text2 = UIElements['NewUserLoginScreen-UIElement-2']['content']['text-to-fill']
    const t3 = UIElements['NewUserLoginScreen-UIElement-3']['content']['text-to-fill']
    const t4 = UIElements['NewUserLoginScreen-UIElement-4']['content']['text-to-fill']
    const text5 = UIElements['NewUserLoginScreen-UIElement-5']['content']['text-to-fill']
    const text6 = UIElements['NewUserLoginScreen-UIElement-6']['content']['text-to-fill']
    const button1 = UIElements['NewUserLoginScreen-UIElement-7']['content']['text-to-fill']
    const button2 = UIElements['NewUserLoginScreen-UIElement-8']['content']['text-to-fill']

    const [text3, setText3] = useState(t3)
    const [text4, setText4] = useState(t4)

    const [error, setError] = useState(false)
    const [done, setDone] = useState(false)

    let data = false
    NetInfo.fetch().then(state => {
        // console.log("Connection type", state.type);
        // console.log("Is connected?", state.isConnected);
        data = state.isConnected
        if (data === false){
            setError(false)
            alert('This app needs an active internet connection to work. Please close this application, turn on your data/wifi connection and try again. Thank you!')
        }
    })
     
    const signup = async () => {
        let temp = await AsyncStorage.getItem('createUser')
        if(temp === "true"){
            
            let res = await signupUser()
            if(res !== -1){
                // console.log(res)
                await AsyncStorage.setItem('newUserScreen','true')
                await setText3(res.Username)
                await setText4(res.Password)
                await AsyncStorage.setItem('createUser','false')
                await init()
                setDone(true)
                setError(false)

            } else {
                setError(true)
                alert('Something went wrong. Try again after sometime.')
            }
        } else {
            let temp1 = await AsyncStorage.getItem('Username')
            let temp2 = await AsyncStorage.getItem('Password')
            setText3(temp1)
            setText4(temp2)
            setDone(true)
        }        
    }
    const handleButtonPress = async () => {
        if (done) {
            await AsyncStorage.setItem('gotoHome','true')
            navigation.navigate('FirstTimeQuestionnaire')
        }
    }

    useEffect(()=>{
        signup()
     },[])
    return ( 
        <View style={styles.view}>
            <View style={styles.advertismentTop}>
            <Advertisment position="top"/>
            </View>
            
            <View style={styles.logo}>
               <LogoHeader/>
            </View>
            <View style={styles.container}>
                <View style={styles.top}>
                    
                    <Text style={styles.text}>{text1}</Text>
                    <Text style={styles.txtBold}>{text3}</Text>
                    <Text style={styles.text}>{text2}</Text>
                    <Text style={styles.txtBold}>{text4}</Text>
                
                </View>
                {/* <View style={styles.space}>

                </View> */}
                <View style={styles.bottom}>

                    <Text style={styles.text}>{text5}</Text>
                    <View style={styles.btn}>
                        <Button buttonText={button2}  btnWidth={260} callback={handleButtonPress}/>
                    </View>
                    <Text style={styles.text}>{text6}</Text>
                    <View style={styles.btn}>
                        <Button buttonText={button1} btnWidth={260} callback={()=>navigation.navigate('ReturningUserLogin')}/>
                    </View>
                
                </View>
            </View>
            <View style={styles.advertismentBottom}>
            <Advertisment/>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    view: {
        flex:1,
        backgroundColor: 'white',
        // alignItems: 'center'
    },
    advertismentTop: {
        flex: 1,
        alignItems: 'center',
    },
    logo: {
        flex: 3,
        // borderColor: 'red',
        // borderWidth: 1,
    },
    container: {
        flex: 5,
    },
    top: {
        flex: 2,
        justifyContent: 'center',
        alignItems: 'center',
        // borderColor: 'red',
        // borderWidth: 1,
    },
    space: {
        flex: 1
    },
    bottom: {
        flex: 5,
        justifyContent: 'space-evenly',
        alignItems: 'center',
        paddingHorizontal: 30,
        // borderColor: 'red',
        // borderWidth:1,
    },
    button: {
        flex: 1,
        marginVertical: 10,
        alignItems: 'center',
    },
    text: {
        textAlign: 'center',
        fontSize: 16
    },
    txtBold: {
        fontWeight: 'bold'
    },
    advertismentBottom: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        // borderColor: 'red',
        // borderWidth:1,
    },
})

const mapStateToProps = (state) => {
    // console.log(state)
    return {UIElements: state}
  }
  export default connect(mapStateToProps,actions)(NewUserLoginScreen)