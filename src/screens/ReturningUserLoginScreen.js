import React, {useState, useEffect} from 'react'
import { View, Text, StyleSheet, TextInput, Image, SafeAreaView, Dimensions} from 'react-native'
import { WebView } from 'react-native-webview'
import Header from '../components/Header'
import Button from '../components/Button'
import LogoHeader from '../components/LogoHeader'
import Advertisment from '../components/Advertisment'
import {connect} from 'react-redux'
import * as actions from '../actions'
import {loginUser} from '../api/covidsanjivaniApi'
import {init} from '../api/covidsanjivaniApi'
const ReturningUserLoginScreen = ({navigation,UIElements}) => {

    
    const text1 = UIElements['ReturningUserLoginScreen-UIElement-1']['content']['text-to-fill']
    const button1 = UIElements['ReturningUserLoginScreen-UIElement-2']['content']['text-to-fill']
    const button2 = UIElements['ReturningUserLoginScreen-UIElement-3']['content']['text-to-fill']
    const editText1 = UIElements['ReturningUserLoginScreen-UIElement-4']['content']['text-to-fill']
    const editText2 = UIElements['ReturningUserLoginScreen-UIElement-5']['content']['text-to-fill']
    
    const [userName, setUserName] = useState('')
    const [password, setPassword] = useState('')
    const [error, setError] = useState(false)
    const [loading, setLoading] = useState(false)

    const handleLoginPress = async() => {
        setError(false)
        setLoading(true)
        let status  = await loginUser(userName,password)
        // await(console.log(status))
        if(status === "success"){
            setLoading(false)
            navigation.navigate('FirstTimeQuestionnaire')
        } else {
            setLoading(false)
            setError(true)
        }         
    }
    useEffect(()=>{
        init()
    })

    return (
        <SafeAreaView style={styles.view}>
        <View style={styles.advertismentTop}>
        <Advertisment position="top"/>
        </View>
        
        <View style={styles.logo}>
           <LogoHeader/>
        </View>
        <View style={styles.container}>
            <View style={styles.top}>    
                <View style={styles.general}>
                    <TextInput value={userName} selectionColor={'black'}  placeholder={editText1} placeholderTextColor='#c7c7c7' style={styles.textInput} autoCapitalize= "none" autoCorrect= {false} onChangeText={(userName)=>{setUserName(userName)}} on/>
                    <TextInput value={password} secureTextEntry selectionColor={'black'} placeholder={editText2} placeholderTextColor='#c7c7c7' style={styles.textInput} autoCapitalize= "none" autoCorrect= {false} onChangeText={(pass) => {setPassword(pass)}}/>       
                    {error
                    ? <Text style={styles.error}>Login Failed!. Please check your username and password and try again.</Text>
                    : null
                    }
                </View>
                <Button buttonText={button1} btnWidth={230} callback={()=> handleLoginPress()} loading={loading}/>
               
            </View>
            {/* <View style={styles.space}>

            </View> */}
            <View style={styles.bottom}>

                <Text>{text1}</Text>
                {/* <Text>You can always make a fresh start!</Text> */}
                <View style={styles.btn}>
                    <Button buttonText={button2} btnWidth={230} 
                    callback={()=>{
                        setUserName('')
                        setPassword('')
                        setError(false)
                        navigation.navigate('NewUserLogin')
                    }}/>
                </View>
            
            </View>
        </View>
        <View style={styles.advertismentBottom}>
        <Advertisment/>
        </View>
    
    </SafeAreaView>
)
}

var {height, width} = Dimensions.get('window')
const styles = StyleSheet.create({
view: {
    height : height,
    width : width,
    backgroundColor: 'white',
},
advertismentTop: {
    flex: 1,
    alignItems: 'center',
},
logo: {
    flex: 2.75,
    // borderColor: 'red',
    // borderWidth: 1,
},
container: {
    flex: 5,
},
top: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    // borderColor: 'red',
    // borderWidth: 1,
},
general: {
    justifyContent: 'center',
    alignItems: 'center',
},
error: {
    color: 'red', 
    textAlign: 'center', 
    paddingHorizontal: 35
},
space: {
    flex: 0
},
bottom: {
    flex: 0.5,
    justifyContent: 'space-evenly',
    alignItems: 'center',
    paddingHorizontal: 30,
    // borderColor: 'red',
    // borderWidth:1,
},
textInput: {
    marginBottom: 20, 
    color: 'black', 
    width: 250,
    borderWidth: 1, 
    borderColor:'white', 
    borderBottomColor: '#3b5998',
    borderTopWidth: 0
    // backgroundColor: 'white'
},
button: {
    flex: 1,
    marginVertical: 10,
    alignItems: 'center',
},
text: {
    textAlign: 'center',
    fontSize: 16
},
txtBold: {
    fontWeight: 'bold'
},
advertismentBottom: {
    flex: 1.75,
    justifyContent: 'center',
    alignItems: 'center',
    // borderColor: 'red',
    // borderWidth:1,
  },
})

const mapStateToProps = (state) => {
    // console.log(state)
    return {UIElements: state}
  }
  export default connect(mapStateToProps,actions)(ReturningUserLoginScreen)