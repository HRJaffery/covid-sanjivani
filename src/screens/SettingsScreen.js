import React, {useEffect, useState} from 'react'
import { View, Text, StyleSheet, AsyncStorage, TouchableOpacity} from 'react-native'
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import LogoHeader from '../components/LogoHeader'
import Button from '../components/Button'
import Advertisment from '../components/Advertisment'
import {connect} from 'react-redux'
import * as actions from '../actions'
import { Dialog, ConfirmDialog } from 'react-native-simple-dialogs';

const SettingsScreen = ({navigation,UIElements}) => {
    const [user, setUser] = useState('')
    const [pass, setPass] = useState('')
    const [showModal, setshowModal] = useState(false)
    const [showConfirm, setshowConfirm] = useState(false)

    const getCredentials = async () => {
        let username = await AsyncStorage.getItem("Username")
        let password = await AsyncStorage.getItem("Password")
        // console.log(username)
        await setUser(username)
        await setPass(password)
    }    
    useEffect(()=>{
        getCredentials()
    })
    // console.log(username)
    const text1 = UIElements['SettingsScreen-UIElement-1']['content']['text-to-fill']
    const text2 = UIElements['SettingsScreen-UIElement-2']['content']['text-to-fill']
    const text3 = UIElements['SettingsScreen-UIElement-3']['content']['text-to-fill']
    const text4 = UIElements['SettingsScreen-UIElement-4']['content']['text-to-fill']
    const text5 = UIElements['SettingsScreen-UIElement-5']['content']['text-to-fill']
    const text6 = UIElements['SettingsScreen-UIElement-6']['content']['text-to-fill']
    const button = UIElements['SettingsScreen-UIElement-7']['content']['text-to-fill']
    return (
        <View style={styles.view}>
            <View style={styles.advertismentTop}>
            <Advertisment position="top"/>
            </View>
            <View style={styles.header}>
                <TouchableOpacity style={{marginRight: 10, padding: 5}} onPress={()=>navigation.openDrawer()}>
                    <MaterialIcons name='menu' color='#3b5998' size={30}/>
                </TouchableOpacity>
            </View>
            <View style={styles.logo}>
               <LogoHeader/>
            </View>
            <View style={styles.container}>
                <View style={styles.general}>
                    <Text>{text1}</Text>
                    <Text style={styles.txtBold}>{user}</Text>
                    {/* <Text>{text2}</Text>
                    <Text style={styles.txtBold}>{pass}</Text> */}
                    <Text></Text>
                </View>
                <View style={styles.general}>
                    <Text style= {{textAlign: 'center'}}>{text5}</Text>
                    <Text style= {{textAlign: 'center'}}>{text6}</Text>
                   
                   <Dialog
                        visible={showModal}
                        title="Password"
                        onTouchOutside={() => setshowModal(false)}
                        
                    >
                        <View>
                        <Text>{pass}</Text>
                        </View>
                    </Dialog>
                    <ConfirmDialog
                        title="Please Confirm"
                        message="Please self attest that you are trying to view your OWN password."
                        visible={showConfirm}
                        animationType = "fade"
                        onTouchOutside={() => setshowConfirm(false)}
                        positiveButton={{
                            title: "I Confirm",
                            onPress: () => {
                                setshowModal(true)
                                setshowConfirm(false)
                                
                                // alert(pass)
                            }
                         }}
                        negativeButton={{
                            title: "Cancel",
                            onPress: () => {
                                setshowConfirm(false)
                                // setshowModal(true)
                            }
                        }}
                    />
                      <ConfirmDialog
                        title={text2}
                        message={pass}
                        animationType = "fade"
                        visible={showModal}
                        onTouchOutside={() => setshowModal(false)}
                        negativeButton={{
                            title: "Cancel",
                            onPress: () => {
                                setshowModal(false)
                                // setshowModal(true)
                            }
                        }}
                    />  

                </View>
                <View style={styles.button}>
                    <Button buttonText={"Show Password"} callback={()=>setshowConfirm(true)}/>
                    {/* <Button buttonText={button} callback = {()=>navigation.navigate('LanguageSelectionDrawer', {showButton: false})}/> */}
                </View>  
            </View>
            <View style={styles.advertismentBottom}>
            <Advertisment/>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    view: {
        flex:1,
        backgroundColor: 'white',
    },
    advertismentTop: {
        flex: 1,
        alignItems: 'center'
    },
    header: {
    },
    logo: {
        flex: 3,
    },
    container: {
        flex: 5,
    },
    advertismentBottom: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    button: {
        flex: 1,
        alignItems: 'center',
        margin: 5
    },
    txtBold: {
        fontWeight: 'bold'
    },
    general: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-evenly',
        borderColor: 'white',
        borderWidth: 1,
        paddingHorizontal: 30
    },
})

const mapStateToProps = (state) => {
    // console.log(state) 
    return {UIElements: state}
  }
  export default connect(mapStateToProps,actions)(SettingsScreen)