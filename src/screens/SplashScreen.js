import React, {useEffect} from 'react'
import { View, StyleSheet, Image, AsyncStorage} from 'react-native'
import {check, PERMISSIONS} from 'react-native-permissions';
import {getAllUIElements} from '../api/covidsanjivaniApi'
import * as actions from '../actions'
import {connect} from 'react-redux'
import NetInfo from "@react-native-community/netinfo"
import Geolocation from '@react-native-community/geolocation';
import * as configs from '../api/covidsanjivaniApi'

const SplashScreen = ({navigation, loadUIElements}) => {
    let data = false
    NetInfo.fetch().then(state => {
        data =  state.isConnected
        if (data === false){
            alert('This app needs an active internet connection to work. Please check your data/wifi connectivity and try again. Thank you!')
        }
    })

    let locationPermissions
    useEffect(()=>{
        // loadUI()
        check(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION)
        .then((res)=>{
            if(res === "denied"){
                locationPermissions = -1
            } else {
                locationPermissions = 1
            }
         
        })    
    })
    
    const launch = async() => {
            
        isFirstLaunch = await AsyncStorage.getItem('isFirstLaunch')
        gotoHome = await AsyncStorage.getItem('gotoHome')
        FirstTimeQuestionnaire = await AsyncStorage.getItem('FirstTimeQuestionnaireScreen')
        newUserScreen = await AsyncStorage.getItem('newUserScreen')
        let pass = await AsyncStorage.getItem('Password')
        return new Promise((resolve) =>
        setTimeout(
          async () => { 
            // alert(isFirstLaunch)
            // if(true){
            if (isFirstLaunch !== 'false' ){
                await AsyncStorage.setItem('createUser','true')
                await AsyncStorage.setItem('isFirstLaunch','false')
                await AsyncStorage.setItem('app-language','English') 
                navigation.navigate('LanguageSelection', {showButton: true})
                        
            } else {
                if (locationPermissions === 1 && pass !== null){
                    navigation.navigate('AppHome')
                } else if (locationPermissions !== 1 && pass !== null){
                    // const resetAction = StackActions.reset({
                    //     index: 0,                       
                    //     actions: [
                    //         NavigationActions.navigate({ routeName: 'LocationPermissions', params: {showButton: true, home: true} }),
                    //         nav
                    //     ],
                    // })
                    // navigation.dispatch(resetAction)
                    navigation.navigate('LocationPermissions',{showButton: true, home: true} )

                } else if (locationPermissions === 1 && FirstTimeQuestionnaire ==='true'){
                    navigation.navigate('AppHome')
                } else if (locationPermissions === 1 && FirstTimeQuestionnaire !=='true' && pass !== null){
                    navigation.navigate('FirstTimeQuestionnaire', {showButton: true})
                } else if (locationPermissions !== 1 && pass === null){
                    navigation.navigate('LanguageSelection', {showButton: true})
                } else if (pass === null){
                    navigation.navigate('ReturningUserLogin', {showButton: true})
                } else {
                    navigation.navigate('AppHome')
                }                  
            } 
        },
          1500
        )
      );
        
    }
    
    const init = async() => {
        await configs.init()
        check(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION)
        .then((res)=>{
            if(res === "denied"){
                locationPermissions = -1
            } else {
                locationPermissions = 1
                Geolocation.getCurrentPosition(
                    info => {
                        lat =  info.coords.latitude
                        long = info.coords.longitude
                        AsyncStorage.setItem("lat", JSON.stringify(lat))
                        AsyncStorage.setItem("long", JSON.stringify(long))
                    }
                );
            }
        })   
       
        let res = await getAllUIElements()
        if (res != -1){
            await loadUIElements(res)
        }   else {
            // console.log("SPLASH",res)
        }

        
    }
    useEffect (()=>{
        
        init()
        launch()  
    })
    

    return (
        <View style={styles.view}>
            <Image style={styles.image} source = {require('../../assets/App-Logo.png')} />
        </View>
    )
}

const styles = StyleSheet.create({
    view: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        // borderWidth: 1,
        // borderColor: 'red'
    },
    image: {
        // flex: 1,
        width: '35%',
        height: '35%',
        aspectRatio: 1,
    }
})

export default connect(null,actions)(SplashScreen)