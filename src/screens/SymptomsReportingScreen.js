import React, {useState, useEffect} from 'react'
import { View, Text, StyleSheet, TouchableOpacity} from 'react-native'
import { WebView } from 'react-native-webview'
import Header from '../components/Header'
import Button from '../components/Button'
import LogoHeader from '../components/LogoHeader'
import SelectMultiple from '../components/react-native-select-multiple-master/src/SelectMultiple'
import Advertisment from '../components/Advertisment'
import {connect} from 'react-redux'
import * as actions from '../actions'
import {reportSymptoms, sendLocationData} from '../api/covidsanjivaniApi'
import { Toast } from 'native-base';
import Geolocation from '@react-native-community/geolocation';
import {check, PERMISSIONS} from 'react-native-permissions';
import { NavigationEvents } from 'react-navigation';

const SymptomsReportingScreen = ({navigation,UIElements}) => {
    const text = UIElements['SymptomsReportingScreen-UIElement-1']['content']['text-to-fill']
    const checkBoxMenu = UIElements['SymptomsReportingScreen-UIElement-2']['content']['list-to-fill']
    const button = UIElements['SymptomsReportingScreen-UIElement-3']['content']['text-to-fill']
    const [selectedSymptoms,setSymptoms]= useState([]);
    const [loading, setLoading] = useState(false)
    const [clean, setClean] = useState(false)
    useEffect(()=>{
        setSymptoms([])
    },[clean])
    const showToast = (message, duration = 5000) => {
        Toast.show({
          text: message,
          duration,
          position: 'bottom',
          textStyle: { textAlign: 'center' },
          style: {alignSelf: 'center', backgroundColor: '#808080',paddingHorizontal:10, marginBottom: 100, borderRadius: 25, width: 350 }
        });
    }

    const onSelectionsChange = (selectedItems) => {
        // selectedSymptoms is array of { label, value }
       const selectedSymptomsArray = []
        selectedItems.forEach(element => {
           selectedSymptomsArray.push(element.value);
        });
        
        setSymptoms(selectedSymptomsArray);
        // console.log(selectedSymptomsArray);
        // console.log(selectedSymptoms);
    }
    const handleButtonPress = async () => {

        check(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION)
        .then((res)=>{
            if(res === "denied"){
                showToast('Thank you for reporting. To continue helping communities fight Covid-19, please allow the app to access location. Your data is completely anonymous.')
            } else {
                Geolocation.getCurrentPosition(
                    info => {
                        // console.log(info.coords.latitude,info.coords.longitude)
                        sendLocationData("SymptomsReportingScreen","foregroud",info.coords.latitude,info.coords.longitude)
                    }
                );
            } 
        }) 
        
        if(selectedSymptoms.length === 0){
            showToast('Please select the symptom(s) your are feeling')
        } else {
            setLoading(true)
            let unSelected = [...checkBoxMenu]
            selectedSymptoms.forEach((selected)=>{
                unSelected = unSelected.filter(item => item !== selected)
            })
            // console.log(checkBoxMenu,selectedSymptoms,unSelected)
            let res = await reportSymptoms(checkBoxMenu,selectedSymptoms,unSelected)
            if (res === "success"){
                setLoading(false)
                setClean(true)
                navigation.navigate('SymptomsResponse')
            } else {
                setLoading(false)
                setClean(true)
                alert('Something went wrong. Please try again.')
            }
        }
    }
 
    return (
        <View style={styles.view}>
            <NavigationEvents
                onWillFocus={()=>{
                    setSymptoms([])
                }}
            />
            <View style={styles.advertismentTop}>
            <Advertisment position="top"/>
            </View>
            <View style={styles.header}>
                <Header leftIcon='menu' leftCallback={()=>navigation.openDrawer()} />
            </View>
            <TouchableOpacity onPress={()=>showToast('hi')}>
            </TouchableOpacity>
            <View style={styles.container}>
                <View style={styles.textView}>
                    <Text style={{fontWeight: 'bold', fontSize: 17}}>{text}</Text>
                </View>
                <View style={styles.listView}>
                    <SelectMultiple
                    items={checkBoxMenu}
                    selectedItems={selectedSymptoms}
                    onSelectionsChange={onSelectionsChange} 
                    />
                </View>
                <View style={styles.buttonView}>
                    <Button buttonText={button}  btnWidth={230} callback={()=> handleButtonPress()} loading = {loading}/>
                </View>

            </View>
            <View style={styles.advertismentBottom}>
            <Advertisment/>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    view: {
        flex:1,
        backgroundColor: 'white',
        // alignItems: 'center'
    },
    advertismentTop: {
        flex: 1,
        alignItems: 'center',
    },
    advertismentBottom: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        // borderColor: 'red',
        // borderWidth:1,
    },
    container: {
        flex: 9,
        // borderColor: 'red',
        // borderWidth: 1,
        paddingHorizontal: 50,
        // alignItems: 'center'
    },
    textView: {
        flex: 1,
        justifyContent: 'center',
        // alignItems: 'center'
    },
    listView: {
        flex: 5,
        borderColor: '#c7c7c7',
        borderWidth: 1,
    },
    buttonView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})

const mapStateToProps = (state) => {
    // console.log(state)
    return {UIElements: state}
  }
export default connect(mapStateToProps,actions)(SymptomsReportingScreen)