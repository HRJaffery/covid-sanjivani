import React, {useState} from 'react'
import { View, Text, StyleSheet, TextInput, Image, KeyboardAvoidingView , Dimensions, SafeAreaView} from 'react-native'
import { WebView } from 'react-native-webview'
import Header from '../components/Header'
import Button from '../components/Button'
import LogoHeader from '../components/LogoHeader'
import SelectMultiple from '../components/react-native-select-multiple-master/src/SelectMultiple'
import RadioForm from 'react-native-simple-radio-button';
import Advertisment from '../components/Advertisment'
import {connect} from 'react-redux'
import * as actions from '../actions'
import {reportTestResults, sendLocationData} from '../api/covidsanjivaniApi'
import { Toast } from 'native-base';
import Geolocation from '@react-native-community/geolocation';
import {check, PERMISSIONS} from 'react-native-permissions';
import { ConfirmDialog } from 'react-native-simple-dialogs';

const setRadioButtonsList = (list) => {
    let temp = []
    let i = 0
    // console.log(list)
    list.forEach(element => {
        let obj = {
            label: element, value: i
        }
        temp.push(obj)
        i = i + 1
    });
    // console.log(temp)
    return temp
} 

const TestResultsReportingScreen = ({navigation,UIElements}) => {

    
    const [selectedValue, setSelectedValue] = useState(1) 
    const [loading, setLoading] = useState(false)
    const [showConfirm, setshowConfirm] = useState(false)
    const [code, setCode] = useState(-1)
    const text1 = UIElements['TestResultsReportingScreen-UIElement-1']['content']['text-to-fill']
    const list = UIElements['TestResultsReportingScreen-UIElement-2']['content']['list-to-fill']
    const button = UIElements['TestResultsReportingScreen-UIElement-3']['content']['text-to-fill']
    const text2 = UIElements['TestResultsReportingScreen-UIElement-4']['content']['text-to-fill']
    const text3 = UIElements['TestResultsReportingScreen-UIElement-5']['content']['text-to-fill']
    const text4 = UIElements['TestResultsReportingScreen-UIElement-6']['content']['text-to-fill']

    const radioButtonsList = setRadioButtonsList(list)
    
    const showToast = (message, duration = 5000) => {
        Toast.show({
          text: message,
          duration,
          position: 'bottom',
          textStyle: { textAlign: 'center' },
          style: {alignSelf: 'center', backgroundColor: '#808080',paddingHorizontal:10, marginBottom: 100, borderRadius: 25, width: 350 }
        });
    }
    
    const handleButtonPress = async (navigation, list, value, code) => {
        
        check(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION)
        .then((res)=>{
            if(res === "denied"){
                showToast('Thank you for reporting. To continue helping communities fight Covid-19, please allow the app to access location. Your data is completely anonymous.')
            } else {
                Geolocation.getCurrentPosition(
                    info => {
                        // console.log(info.coords.latitude,info.coords.longitude)
                        sendLocationData("TestResultsReportingScreen","foregroud",info.coords.latitude,info.coords.longitude)
                    }
                );
            } 
        }) 
        
       
        setLoading(true)
        let unSelected = [...list]
        unSelected.splice(value,1)
        let selected = []
        selected.push(list[value])
        var res = 0
        if (code === -1){
            res = await reportTestResults(list, selected, unSelected)
        } else {
            res = await reportTestResults(list, selected, unSelected, code)
        }
        if (res === "success"){
            setLoading(false)
            navigation.navigate('TestResultsResponse')
        } else {
            setLoading(false)
            alert('Something went wrong. Please try again.')
        }
        
    }

    return (
        <SafeAreaView style={styles.view}>
            
            <View style={styles.advertismentTop}>
            <Advertisment position="top"/>
            </View>
            <View style={styles.header}>
                <Header leftIcon='menu' leftCallback={()=>navigation.openDrawer()} />
            </View>
            <View style={styles.container}>
                <View style={styles.textView}>
                    <Text style={styles.textStyle}>{text1}</Text>
                </View>
                <View style={styles.textInputView}>
                    <TextInput onFocus={()=>{}} selectionColor={'black'}  placeholder={text2} placeholderTextColor='#c7c7c7' style={styles.textInput} autoCapitalize= "none" autoCorrect= {false} onChangeText={(code)=>{setCode(code)}}/>
                </View>
                <View style={styles.radioView}>
                    <RadioForm
                    radio_props={radioButtonsList}
                    initial={0}
                    onPress={(value) => {setSelectedValue(value)}}
                    buttonColor = {'#3b5998'}
                    selectedButtonColor  = '#3b5998'
                    buttonSize={15}
                    />
                </View>
                <View style={styles.textView}>
                    <Text style={styles.textStyle}>{text4}</Text>
                </View>
                <View style={styles.buttonView}>
                <ConfirmDialog
                        title="Please Confirm"
                        message="Please confirm you wish to report your test results."
                        visible={showConfirm}
                        animationType = "fade"
                        onTouchOutside={() => setshowConfirm(false)}
                        positiveButton={{
                            title: "I Confirm",
                            onPress: () => {
                                setshowConfirm(false)
                                handleButtonPress(navigation, list, selectedValue, code)
                                
                            }
                         }}
                        negativeButton={{
                            title: "Cancel",
                            onPress: () => {
                                setshowConfirm(false)
                                // setshowModal(true)
                            }
                        }}
                    />
                    <Button buttonText={button}  btnWidth={230} callback={()=>setshowConfirm(true)} loading={loading}/>
                </View>
                <View style={styles.textView}>
                    <Text style={styles.textStyle}>{text3}</Text>
                </View>
            </View>
            <View style={styles.advertismentBottom}>
            <Advertisment/>
            </View>
        </SafeAreaView>
    )
}
var {height, width} = Dimensions.get('window')
const styles = StyleSheet.create({
    view: {
        height : height,
        width : width,
        backgroundColor: 'white',
        
        // alignItems: 'center'
    },
    advertismentTop: {
        flex: 1,
        alignItems: 'center',
    },
    advertismentBottom: {
        flex: 2,
        justifyContent: 'center',
        alignItems: 'center',
        // borderColor: 'red',
        // borderWidth:1,
    },
    container: {
        flex: 9,
        // borderColor: 'red',
        // borderWidth: 1,
        paddingHorizontal: 45,
        // alignItems: 'center',
        // justifyContent: 'center'
    },
    textView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        // borderColor: 'red',
        // borderWidth: 1,
    },
    radioView: {
        flex: 1,
        // borderColor: '#c7c7c7',
        // borderWidth: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        // marginTop: 10,
        // borderColor: 'red',
        // borderWidth: 1,
    },
    textInputView: {
        flex:1,
        justifyContent: 'center',
        alignItems: 'center',
        // borderColor: 'red',
        // borderWidth: 1,
    },
    textInput: {
        marginTop: 10,
        // marginBottom: 20, 
        color: 'black', 
        width: 310,
        borderWidth: 1, 
        borderColor:'white', 
        borderBottomColor: '#3b5998',
        // backgroundColor: 'white'
    },
    textStyle: {
        textAlign: 'center',
        fontSize: 16
    }
})

const mapStateToProps = (state) => {
    // console.log(state)
    return {UIElements: state}
  }
  export default connect(mapStateToProps,actions)(TestResultsReportingScreen)