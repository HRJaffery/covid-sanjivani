import React, {useState} from 'react'
import { View, StyleSheet, Dimensions, TouchableOpacity, ActivityIndicator, Text} from 'react-native'
import { WebView } from 'react-native-webview'
import Header from '../components/Header'
import { TestResultsResponseWebView } from '../api/covidsanjivaniApi'
import Advertisment from '../components/Advertisment'
import {connect} from 'react-redux'
import * as actions from '../actions'
import MaterialIcons from "react-native-vector-icons/MaterialIcons";

const TestResultsResponseScreen = ({navigation,UIElements}) => {

    const [LoadingError, setLoadingError] = useState(false)
    const INJECTEDJAVASCRIPT = "document.body.style.userSelect = 'none'";
    const [show, setShow] = useState(false)
    return (
        <View style={styles.view}>
            <View style={styles.advertismentTop}>
            <Advertisment position="top"/>
            </View>
            <View style={styles.header}>
                <Header leftIcon='menu' leftCallback={()=>navigation.openDrawer()} />
            </View>
            <View style={styles.container}>
            {!LoadingError
      ?
      <>
                <WebView 
                    source={{ uri: TestResultsResponseWebView }}  
                    onLoad = {() => setShow(true)}
                    injectedJavaScript={INJECTEDJAVASCRIPT}
                    onError = {()=>{setLoadingError(true)}}
                />
                {!show
                    
                    ?
                    <ActivityIndicator style={styles.loading} size="large"  color= '#3b5998'/>
                
                    : null
                }
                </>
                    : <View style={{justifyContent: 'center', alignItems:'center', paddingHorizontal: 35}}>
                        <Text style={{textAlign: 'center'}}>Something went wrong. Please check your internet connection and try again.</Text>
                        <TouchableOpacity style={{marginRight: 10, padding: 5}} 
                        onPress={()=>{
                            setLoadingError(false)
                        }}>
                            <MaterialIcons name='refresh' color='#3b5998' size={30}/>
                        </TouchableOpacity>
                    </View>

                    }
            </View>
            <View style={styles.advertismentBottom}>
            <Advertisment/>
            </View>
        </View>
    )
}
var {height, width} = Dimensions.get('window')
const styles = StyleSheet.create({
    view: {
        flex:1,
        backgroundColor: 'white',
        // alignItems: 'center'
    },
    advertismentTop: {
        flex: 1,
        alignItems: 'center',
    },
    advertismentBottom: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        // borderColor: 'red',
        // borderWidth:1,
    },
    header: {
        // borderColor: 'red',
        // borderWidth: 1,
        flex: 1
    },  
    container: {
        flex: 9,
        justifyContent: 'center',
        // borderColor: 'red',
        // borderWidth: 1,
        // paddingHorizontal: 50,
        // alignItems: 'center'
    },
    loading: {
        position: "absolute", 
        top: height / 3, 
        left: width / 2.17,
    },
})

const mapStateToProps = (state) => {
    // console.log(state)
    return {UIElements: state.UIElements}
  }
  export default connect(mapStateToProps,actions)(TestResultsResponseScreen)