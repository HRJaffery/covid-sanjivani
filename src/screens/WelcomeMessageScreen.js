import React from 'react'
import { View, Text, StyleSheet, TouchableOpacity} from 'react-native'
import LogoHeader from '../components/LogoHeader'
import {connect} from 'react-redux'
import * as actions from '../actions'

const WelcomeMessageScreen = ({navigation,UIElements}) => {
    const text = UIElements['WelcomeMessageScreen-UIElement-1']['content']['text-to-fill']
    return (
        <TouchableOpacity style={styles.view} onPress={()=>{navigation.navigate('AboutApp', {showButton: true})}}>
            {/* <View style={styles.view}> */}
                <View style={styles.logoView}>
                    <View style={styles.space}>

                    </View>
                    <View style={styles.logo}>
                        <LogoHeader/>
                    </View>
                </View>
                <View style={styles.message}>
                    <Text  style={styles.text}> {text}</Text>
                    
                    {/* <View style={styles.button}>
                        {showButton
                        ?
                        <Button buttonText="Continue" callback = {()=>navigation.navigate('WelcomeMessage', {showButton: true})}/>
                        : null
                        }
                    </View>   */}
                </View>
            {/* </View>     */}
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    view: {
        flex: 1,
        backgroundColor: 'white'
    },
    logoView:{
        flex: 3,
        // borderColor: 'red',
        // borderWidth: 1
    },
    space: {
        flex:2
    },
    logo:{
        flex: 3,
        paddingBottom: 20,
        // borderColor: 'red',
        // borderWidth: 1
    },
    message: {
        flex: 2,
        alignItems: 'center',
        backgroundColor: 'white',
        // paddingTop: 20,
        // borderColor: 'red',
        // borderWidth: 1,
    },
    text: {
        textAlign: 'center',
        fontSize: 16
    },
    button: {
        flex: 1,
        marginVertical: 10,
        alignItems: 'center',
    },
})

const mapStateToProps = (state) => {
    // console.log(state)
    return {UIElements: state}
  }
  export default connect(mapStateToProps,actions)(WelcomeMessageScreen)